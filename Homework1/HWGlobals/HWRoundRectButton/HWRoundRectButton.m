//
//  HWRoundRectButton.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWRoundRectButton.h"

@implementation HWRoundRectButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setTitleColor:[UIColor hw_colorWithHexInt:HWLeafColorHex] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor hw_colorWithHexInt:HWGrayColorHex] forState:UIControlStateDisabled];
        
        self.showsTouchWhenHighlighted = YES;
        self.layer.borderColor = [UIColor hw_colorWithHexInt:HWLeafColorHex].CGColor;
        self.layer.borderWidth = 2.0f;
        self.layer.cornerRadius = 5.0f;
    }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size {
    return CGSizeMake((size.width <= CGSizeZero.width || size.width >= HWDefaultButtonWidht ? HWDefaultButtonWidht : size.width), size.height);
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    
    self.layer.borderColor = enabled ? [UIColor hw_colorWithHexInt:HWLeafColorHex].CGColor : [UIColor hw_colorWithHexInt:HWGrayColorHex].CGColor;

}


@end
